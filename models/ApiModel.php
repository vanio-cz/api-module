<?php

namespace Vanilla\ApiModule;

/**
 * Api database Model
 * 
 * @author			Pavel Lauko
 * @package		ApiModule
 */
class ApiModel extends \Nette\Object {

	/** @var string */
	private static $HASH_SALT_ACCESS_TOKEN = 'gkajSK7ptmc5dShUNr34GprN13MyKvaZoy';
	
	/** @var string */
	private static $HASH_SALT_API_KEY = 'cvH9niW7fhEmkU9G12bIwzktsy38xP';

	/** @var Nette\Database\Connection */
	private $db;

	
	/**
	 * 
	 * @param \Nette\Database\Connection $db
	 */
	public function __construct(\Nette\Database\Connection $db) {
		$this->db = $db;
	}

	
	/**
	 * 
	 * @param string $accessToken
	 * @return boolean|string
	 */
	public function validateAccessToken($accessToken) {
		$tokenRow = $this->db->table("api_access_tokens")->where('token', $accessToken)->where('expires>NOW()')->fetch();
		if (!$tokenRow)
			return false;
		else
			return $tokenRow['users_id'];
	}
	

	/**
	 * 
	 * @param string $userName
	 * @param string $apiKey
	 * @return boolean|string
	 */
	public function checkApiKey($userName, $apiKey) {
		if ($userName == null || $apiKey == null)
			return false;

		return ($this->getApiKey($userName) === $apiKey);
	}

	
	/**
	 * 
	 * @param string $userName
	 * @param int $expiration
	 * @return boolean|string
	 */
	public function getAccessToken($userName, $expiration) {

		// delete expired tokens
		$this->deleteExpiredTokens();
		$token = hash('sha256', time() . $userName . str_repeat(self::$HASH_SALT_ACCESS_TOKEN, 10));
		$expires = new \DateTime;
		$expires->modify('+ ' . $expiration . ' seconds');

		$user = $this->db->table('users')->select('*')->where('username', $userName)->fetch();
		$userId = $user['id'];

		$newToken = array(
			'users_id' => $userId,
			'expires' => $expires,
			'token' => $token
		);
		if ($this->db->table('api_access_tokens')->insert($newToken))
			return $token;
		else
			return false;
	}
	

	/**
	 * 
	 */
	public function deleteExpiredTokens() {
		$this->db->table('api_access_tokens')->where('expires<NOW()')->delete();
	}
	
	
	/**
	 * 
	 * @param int $userId
	 * @return string
	 */
	public function regenerateApiKey($userId) {
		$apiKey = hash('sha256', time() . $userId . str_repeat(self::$HASH_SALT_API_KEY, 8));
		$user = $this->db->table('users')->get($userId);
		if ($user->update(array('api_key' => $apiKey)))
			return $apiKey;
	}

	/**
	 * 
	 * @param string $userName
	 * @return string
	 */
	public function getApiKey($userName) {
		$user = $this->db->table('users')->where('username', $userName)->fetch();
		return $user['api_key'];
	}
	
	
	protected function getDb(){
		return $this->db;
	}
	
	
	/**
	 * Returns item by ID 
	 * @param int $id
	 * @return
	 */
	public function get($id) {
		$result = $this->db->table(self::$TABLE)->where('id', $id)->fetch();	
		return $result; 
	}

}
