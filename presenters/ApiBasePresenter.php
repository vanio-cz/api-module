<?php
/**
 * Api Module
 */
namespace Vanilla\ApiModule;

use \Nette\Application\Responses\JsonResponse;
use \Nette\Http\Response;

/**
 * API Base Presenter
 * 
 * @package Fakturant
 * @subpackage API
 */
abstract class ApiBasePresenter extends \Nette\Application\UI\Presenter {

	/**
	 * Expiration time in seconds
	 * @var int
	 */
	public static $TOKEN_EXPIRATION_TIME = 3000;
	public static $DATA_PART = 'data';
	public static $RESULT_PART = 'result';
	public static $STATUS_PART = 'status';
	public static $MESSAGE_PART = 'message';
	public static $SUCCESS_PART = 'success';
	public static $ERROR_PART = 'error';
	public static $ACCESS_TOKEN_PART = 'accessToken';
	public static $TOKEN_EXPIRATION_PART = 'tokenExpiration';
	
	const NOT_FOUND = 'Requested entity not found';
	const WRONG_ARGS = 'Wrong or missing arguments';
	const METHOD_NOT_ALLOWED = 'Requested method not allowed';
	const NOT_AVAILABLE = 'Requested resource is not available.';

	/** @var ApiModel */
	private $model;
	private $apiUserId = null;
	protected $apiResponse = array();

	public function __construct(\Nette\DI\IContainer $context) {
		$this->model = new ApiModel($context->database);
		
		//Debugger::enable(Debugger::PRODUCTION);
		$context->httpResponse->setHeader('Pragma', 'no-cache');
		parent::__construct($context);
	}

	/**
	 * Check access token before action
	 */
	protected function startup() {
		// check for everything except getToken
		if ($this->getAction() != 'getAccessToken' && $this->getAction() != 'default') {
			$accessToken = $this->getParam('accessToken');
			
			$this->apiUserId = $this->model->validateAccessToken($accessToken);
			if ($this->apiUserId != false) {
				$this->apiResponse = $this->getBaseReply(true, 'Authorization successfull. Access token valid.');
			} else {
				$this->context->httpResponse->setCode(Response::S401_UNAUTHORIZED);
				$this->apiResponse = $this->getBaseReply(false, 'Unauthorized access. Wrong access token.');
				$this->sendResponse(new JsonResponse($this->apiResponse));
			}
		}
		parent::startup();
	}

	/**
	 * Endpoint for token retrieval
	 * @param type $userName
	 * @param type $apiKey
	 */
	public function actionGetAccessToken($userName, $apiKey) {
		if ($this->model->checkApiKey($userName, $apiKey)) {
			$this->apiResponse = $this->getBaseReply(true, 'Authentication successfull. Access token sent.');
			$this->apiResponse[self::$ACCESS_TOKEN_PART] = $this->model->getAccessToken($userName, self::$TOKEN_EXPIRATION_TIME);
			$this->apiResponse[self::$TOKEN_EXPIRATION_PART] = self::$TOKEN_EXPIRATION_TIME;
		} else {
			$this->apiResponse = $this->getBaseReply(false, 'Authentication failed.');
		}
	}

	/**
	 * Returns post parameters
	 * @return type
	 */
	protected function getPostParams() {
		return $this->context->httpRequest->getPost();
	}

	/**
	 * Returns base response with status and message
	 * @param boolean $success
	 * @param string $message
	 * @return array
	 */
	private function getBaseReply($success, $message) {
		$reply = array(
			self::$RESULT_PART => array(
				self::$STATUS_PART => $success ? self::$SUCCESS_PART : self::$ERROR_PART,
				self::$MESSAGE_PART => $message
			)
		);
		return $reply;
	}

	/**
	 * Sets the result part of response
	 * @param type $data
	 */
	protected function setData($data) {
		$this->apiResponse[self::$DATA_PART] = $data;
	}

	/**
	 * Returns ID of authenticated user (token owner)
	 * @return type
	 */
	protected function getApiUserId() {
		return $this->apiUserId;
	}
	
	
	/**
	 * Set Response, wrong arguments
	 */
	protected function setWrongArgumentResponse() {
		$this->context->httpResponse->setCode(Response::S400_BAD_REQUEST);
		$this->apiResponse = $this->getBaseReply(false, $this::WRONG_ARGS);
	}
	
	/**
	 * Set Response, method not allowed
	 */
	protected function setMethodNotAllowed($allowed_method) {
		$this->context->httpResponse->setCode(Response::S405_METHOD_NOT_ALLOWED);
		$this->context->httpResponse->setHeader('Allow', $allowed_method);
		$this->apiResponse = $this->getBaseReply(false, $this::METHOD_NOT_ALLOWED);
	}
	
	/**
	 * Set Response, not found
	 */
	protected function setNotFound() {
		$this->context->httpResponse->setCode(Response::S404_NOT_FOUND);
		$this->apiResponse = $this->getBaseReply(false, $this::NOT_FOUND);
	}
	
	/**
	 * Check Allowed Params
	 * 
	 * @param array $data
	 * @param array $allowedParams
	 * @return boolean
	 */
	protected function checkAllowedParams($data,$allowedParams) {			
		$params_ok = false;
		$params = array();
		foreach ($data as $name=>$value) {				
			if(in_array($name, $allowedParams)) { 
				$params[] = true;
				break;
			}
		}
		return ( count($allowedParams) == count($allowedParams) );
	}
	

	/**
	 * Sends api response instead of rendering
	 */
	protected function beforeRender() {

		if ($this->getAction() != 'default') {

			// function defination to convert array to xml
			function array_to_xml($student_info, &$xml_student_info) {
				foreach ($student_info as $key => $value) {
					if (is_array($value)) {
						if (!is_numeric($key)) {
							$subnode = $xml_student_info->addChild("$key");
							array_to_xml($value, $subnode);
						} else {
							array_to_xml($value, $xml_student_info);
						}
					} else {
						$xml_student_info->addChild("$key", "$value");
					}
				}
			}

			if ($this->getParam('output') == 'xml') {
				$data = array($this->apiResponse);
				$xml_data = new SimpleXMLElement("<?xml version=\"1.0\"?><response></response>");
				array_to_xml($data, $xml_data);
				header("Content-type: text/xml");
				echo $xml_data->asXML();
				$this->terminate();
			} else {
				$this->sendResponse(new JsonResponse($this->apiResponse));
			}
		} else {
			$this->setLayout(false);
		}
		parent::beforeRender();
	}

}
